import React, {Component} from 'react';

import './MovieInput.css'

class MovieInput extends Component {
  render() {
    return (
      <form action="#">
        <input
          type="text"
          className="movie"
          placeholder='Введите фильм который Вы хотите добавить после выхода в свою коллекцию'
          onChange={this.props.saveMovie}
          value={this.props.value}
        />
        <button className='button' onClick={this.props.submitMovie}>Add movie</button>
      </form>
    );
  }
}

export default MovieInput;