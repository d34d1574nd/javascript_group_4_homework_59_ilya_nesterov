import React from 'react';

import './HeaderMovie.css';

import logo from '../../accets/Kinopoisk.png';

const HeaderMovie = () => {
  return (
    <header className="header">
      <img src={logo} className="logo" alt="logo" />
      <p className='text'>Movies that I'm waiting for</p>
    </header>
  );
};

export default HeaderMovie;