import React, {Component} from 'react';

import './MovieList.css'

class MovieList extends Component {

  shouldComponentUpdate (nextProps) {
    return (
      nextProps.movie !== this.props.movie
    )
  }

  render() {
    return (
    <div className="movieItem">
      <input className="movieToList" type="text" value={this.props.movie} onChange={(event) => this.props.changer(event, this.props.index)} />
      <button className="close" onClick={this.props.removeMovie}>x</button>
    </div>
    );
  }
}

export default MovieList;