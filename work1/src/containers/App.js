import React, { Component } from 'react';
import './App.css';
import MovieInput from "../components/MovieInput/MovieInput";
import MovieList from "../components/MovieList/MovieList";
import HeaderMovie from "../components/HeaderMovie/HeaderMovie";

class App extends Component {
  state = {
      value: '',
      movieList: [],
      movieId: 0,
    };

  saveMovie = (event) => {
    this.setState({
      value: event.target.value
    });
  };

  submitMovie = () => {
    if (this.state.value === '') {
      return false
    } else {
      let list = [...this.state.movieList];
      const movie = {id: this.state.movieId + 1, movie: this.state.value};

      list.push(movie);
      this.setState({
        value: ' ',
        movieList: list,
        movieId: this.state.movieId + 1,
      });
    }
  };


  removeMovie = id => {
    const list = [...this.state.movieList];
    const index = list.findIndex(task => task.id === id);
    list.splice(index, 1);

    this.setState({
      movieList: list,
      movieId: this.state.movieId - 1
    });
  };


  changer = (event, index) => {
    let movieCopy = this.state.movieList;
    movieCopy[index].movie = event.target.value;
    this.setState({movieList: movieCopy})
  };

  render() {
    let movieList = this.state.movieList.map((item, id) => {
      return (
        <MovieList
          key={item.id}
          movie={item.movie}
          removeMovie={() => this.removeMovie(item.id)}
          changer={this.changer}
          index={id}
        />
      )
    });

    return (
      <div className="App">
        <HeaderMovie />
        <MovieInput
          saveMovie={event => this.saveMovie(event)}
          submitMovie={() => this.submitMovie()}
          value={this.state.value}
        />
        <div className='movieList'>
        {movieList}
        </div>
      </div>
    );
  }
}

export default App;
