import React, { Component } from 'react';
import './App.css';
import Button from "../components/Button/Button";
import AnekdotList from "../components/AnekdotList/AnekdotList";

class App extends Component {
  state = {
    joke: {},
  };

  joke = () => {
    fetch('https://api.chucknorris.io/jokes/random').then(response => {
      if(response.ok) {
        return response.json()
      }
      throw  new Error('Something wrong with request');
    }).then(post => {
      const updatedJoke = {value: post.value};
      this.setState({joke: updatedJoke});
    }).catch(error => {
      console.log(error);
    })
  };

  componentDidMount() {
    this.joke();
  }

  render() {

    return (
      <div className="App">
        <Button
          joke={() => this.joke()}
        />
        <AnekdotList>
          {this.state.joke.value}
        </AnekdotList>
      </div>
    );
  }
}

export default App;
