import React, {Component} from 'react';

import './AnekdotList.css'

class AnekdotList extends Component {
  render() {
    return (
        <article>
          <p className='Post'>{this.props.children}</p>
        </article>
    );
  }
}

export default AnekdotList;